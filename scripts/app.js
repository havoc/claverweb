var app = angular.module('app', ['ngAnimate']);

// services

app.factory('movieService', ['$http', '$sce', function($http, $sce){

    // private

    var _quotesApiUrl = "data/quotes.json";
    var _allQuotes;
    var _fetchAll = function(){
        return $http.get(_quotesApiUrl).then(function (response) {
            _allQuotes = response.data.quotes;
            return _allQuotes;
        });
    };

    var _addAssetsUrl = function(item){
            item.imageUrl = "images/movies/clips/" + item.imageName;
            item.videoUrl = $sce.trustAsResourceUrl("videos/movies/" + item.videoName);
            return item;
    };

    var _init = function() {

    };

    _init();

    // public

    var getPopularQuotes = function() {
        return _fetchAll().then(function (data){
            return Lazy(data)
                .first(10)
                .map(_addAssetsUrl)
                .toArray();
        });
    };

    var search = function(query) {
        return _fetchAll().then(function (data){
            return Lazy(data)
                .filter(function (item) {
                    return item.quote.indexOf(query) >= 0;
                })
                .map(_addAssetsUrl)
                .toArray();
        });
    };

    return {
        getPopularQuotes : getPopularQuotes,
        search : search
    }
}]);

// controllers

app.controller('appController', ['$scope', '$timeout', 'movieService', function($scope, $timeout, movieService){
    // public

    $scope.message = 'Hello World !!'

    $scope.query = "";
    $scope.didSearchClick = function(){
        _doSearch($scope.query);
    }

    $scope.didShowModalClick = function(from, index) {
        $scope.activeItem = from == 0 ? $scope.searchResult[index] : $scope.popularQuotes[index];
        $scope.showModal = true;
    }

    $scope.didCloseModalClick = function() {
        $scope.showModal = false;
        $scope.playClip = false ;
    }

    $scope.didPlayClipClick = function() {
        $scope.playClip = true;
    }

    // watches

    $scope.$watch('query', function (query) {
        if (searchTimeout) $timeout.cancel(searchTimeout);
        if (query.length <= 3) {
            $scope.searchResult = [];
            return;
        }
        var searchTimeout = $timeout(function() {
            _doSearch(query);
        }, 200); // delay 250 ms
    })

    // private

    var _doSearch = function(query) {
        console.log('Searching - ' + query);
        movieService.search(query).then(function (data){
            $scope.$applyAsync(function(){
                $scope.searchResult = data;
            });
        });
    }

    var _getPopularQuotes =  function(){
        movieService.getPopularQuotes().then(function (data){
            $scope.popularQuotes = data;
        });
    }

    var _init = function() {
        _getPopularQuotes();
    };

    _init();
}]);

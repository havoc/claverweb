var tabWidths = [];
var tabWidthRunningSum = [];
var navOffsets = [];
var tabs = [];
var slides = [];
var captions = [];
var downIndex = 0;
var endValue = 0;
var viewportWidth = 0;
var viewportHeight = 0;
var nav;
var imageWidths = [968,2400,800,2600,968,968,2400,2400];
var imageHeights = [1864,1536,1220,2600,1864,1864,1536,1536];

var springSystem = new rebound.SpringSystem();
var mainSpring = springSystem.createSpring();
var downSpring = springSystem.createSpring();

var lastX = 0;
var panVelocity = 0;
var isDragging = false;

$(document).ready(function($) {
	viewportWidth = $("#wrapper").innerWidth();
	viewportHeight = $("#slides li").innerHeight();

	var totalWidth = calculateTabWidths();

	nav = document.getElementById('nav');

	// Size the container to fit
	$("#nav").width(totalWidth);

	$("#nav li").each(function(i, val) {
		navOffsets[i] = navOffsetForIndex(i);
		tabs[i] = val;
	});
 
	setupMainSpring();
	setupPanSupport();
	setupTabPressedStates();

	$("#slides li").each(function(i, val) {
		val.style['webkitTransform'] = 'translate3d(' + viewportWidth * i + 'px, 0, 0)';	
		val.style['MozTransform'] = 'translate3d(' + viewportWidth * i + 'px, 0, 0)';
		slides[i] = val;
	});
	
	// Select the first tab
	selectTabIndex(0, false);

	// Behavior when the tabs are clicked
	var down = [];

	$("#wrapper").mouseup(function() {
		down = [];
	});
});

var activeSlide = 0;

setupMainSpring = function() {
	mainSpring.setSpringConfig(rebound.SpringConfig.fromQcTensionAndFriction(4.5, 5.7));
	
	mainSpring.addListener({	
	    onSpringUpdate: function (spring) {
	    	// Progress from 0 to n
	    	var progress = spring.getCurrentValue();
			console.log(progress);
			// Other transitions
			$("#slides li").each(function(i, val) {
				var slideProgress = 1 - Math.abs(progress - i);
				viewportWidth = viewportWidth - 10;
				var xx = (i * viewportWidth) - (progress * viewportWidth);
				console.log(i, slideProgress, progress, xx, activeSlide);
				// Slide and scale the images
				if (slideProgress > 0) { // Only bother if the slide is visible
					// Slide and scale
					var x = (i * viewportWidth) - (progress * viewportWidth);
					if (x == 0) {
						activeSlide = i;
					}
					//if (i + 1 == activeSlide) {
					//	x = x + 100;
					//}
					//if (i - 1 == activeSlide) {
					//	x = x - 100;
					//}
					var scale = transitionForProgressInRange(slideProgress,0.6,1.0);
					val.style['webkitTransform'] = 'translate3d(' + x + 'px, 0, 0) scale(' + scale +')';
					val.style['MozTransform'] = 'translate3d(' + x + 'px, 0, 0) scale(' + scale +')';
				}

					val.style['opacity'] = 1.0;
					//// Hide the off-screen images so they don't reveal themselves if you resize the browser
					//val.style['opacity'] = (slideProgress > 0) ? 1.0 : 0.0;

			});
		}        
	});
}

setupPanSupport = function() {
	var item = document.getElementById('slides');
	item.addEventListener('touchstart', function(e) {
		var touch = e.touches[0];
		startDragging(touch.pageX);
	}, false);
	
	item.addEventListener('touchmove', function(e) {
	    e.preventDefault(); // Stop vertical rubberbanding on iOS
	
		var touch = e.touches[0];	
		continueDragging(touch.pageX);
	}, false);
	
	item.addEventListener('touchend', function(e) {
		endDragging();
	}, false);
	
	item.addEventListener('touchcancel', function(e) {
		endDragging();
	}, false);
	
	item.addEventListener('mousedown', function(e) {
		startDragging(e.clientX);
	}, false);
	
	item.addEventListener('mousemove', function(e) {
		if (isDragging)
			continueDragging(e.clientX);
	}, false);
	
	item.addEventListener('mouseup', function(e) {
		endDragging();
	}, false);
	
	item.addEventListener('mouseleave', function(e) {
		if (isDragging)
			endDragging();
	}, false);
}

startDragging = function(x) {
	lastX = x;
	isDragging = true;
	viewportWidth = $("#wrapper").innerWidth();
	mainSpring.setAtRest();
}

continueDragging = function(x) {
	panVelocity = x - lastX;
	lastX = x;
	
    var progress = progressForValueInRange(panVelocity,0,-viewportWidth);
    
    var currentValue = mainSpring.getCurrentValue();
    
    // Rubberband when beyond the scroll boundaries
    if ((currentValue + progress) < 0 || (currentValue + progress) > tabs.length - 1)
    	progress *= 0.5;
    
    mainSpring.setCurrentValue(currentValue + progress);
	mainSpring.setAtRest();
}

endDragging = function() {
	var currentPosition = mainSpring.getCurrentValue();
	var restPosition = endValue;
	
	var passedVelocityTolerance = (Math.abs(panVelocity) > 3);
	var passedDistanceTolerance = Math.abs(currentPosition - restPosition) > 0.3;
	var shouldAdvance = passedDistanceTolerance || passedVelocityTolerance;
	var advanceForward = (panVelocity <= 0);
	
	if (shouldAdvance) {
		var targetIndex = advanceForward ? restPosition + 1 : restPosition - 1;
		selectTabIndex(targetIndex, true);
	} else {
		selectTabIndex(restPosition, true);	    	
	}
	
	var normalizedVelocity = progressForValueInRange(panVelocity,0,-viewportWidth);
	mainSpring.setVelocity(normalizedVelocity * 30);
	panVelocity = 0;
	isDragging = false;
}


setupTabPressedStates = function() {
	downSpring.setSpringConfig(rebound.SpringConfig.fromQcTensionAndFriction(100, 5.5));
	
	downSpring.addListener({
		onSpringUpdate: function (spring) {
			var progress = spring.getCurrentValue();
			var scale = transitionForProgressInRange(progress,1.0,0.92);
			tabs[downIndex].style['webkitTransform'] = 'scale('+scale+')';
			tabs[downIndex].style['MozTransform'] = 'scale('+scale+')';
		}
	});
}

pressDownOnTabIndex = function(index) {
	downIndex = index;

	$("#nav li").each(function(i, val) {
		if (i === index) {
			downSpring.setEndValue(1);
		}
	});
}

releaseDownOnTabIndex = function(index) {
	$("#nav li").each(function(i, val) {
		if (i === index) {
			downSpring.setEndValue(0);
		}
	});
}

selectTabIndex = function(i, animated) {
	if (i < 0)
		i = 0;
	else if (i > tabs.length - 1)
		i = tabs.length - 1;

	if (animated) {
		viewportWidth = $("#wrapper").innerWidth();
		endValue = i;
		mainSpring.setEndValue(i);
	} else {
		mainSpring.setCurrentValue(i);
	}
}

navOffsetForIndex = function(i) {
	if (i > 0) {
		var offset = (tabWidthRunningSum[i-1] + (tabWidths[i] / 2.0)) * -1;
	} else {
		var offset = ((tabWidths[i] / 2.0)) * -1;
	}
	return offset;
}

calculateTabWidths = function() {
	var totalWidth = 0;
	
	$("#nav li").each(function(i, val) {
	    tabWidths[i] = $(val).innerWidth();
	    tabWidthRunningSum[i] = tabWidths[i];
	    
	    if (i > 0) {
		    tabWidthRunningSum[i] = tabWidthRunningSum[i] + tabWidthRunningSum[i-1];
	    }
	    
	    totalWidth += tabWidths[i];
	});
	
	return totalWidth;
}

calculateContentScaleForIndex = function(i) {
	var contentWidth = imageWidths[i];
	var contentHeight = imageHeights[i];

	var scale = ((viewportWidth / viewportHeight) > (contentWidth / contentHeight)) ? (viewportHeight / contentHeight) : (viewportWidth / contentWidth);
	return scale;
}


// Utilities

progressForValueInRange = function(value, startValue, endValue) {
	return (value - startValue) / (endValue - startValue);
}

transitionForProgressInRange = function(progress, startValue, endValue) {
	return startValue + (progress * (endValue - startValue));
}

clampedProgress = function(progress) {
	if (progress < 0)
		progress = 0;
	else if (progress > 1)
		progress = 1;
		
	return progress;
}

// Progress: Float value from 0 - # of steps
// Steps: Array of step values 
//
// Example:
// Progress: 1.5, Steps: 100, 200, 250, Result: 225

transitionForProgressInSteps = function(progress, steps) {
	var transition = -1;
	
	// Bail if there's fewer than two steps
	if (steps.length < 2) { return transition };
	
	// If the progress is before the beginning of the range, extrapolate from the first and second steps.
	if (progress < 0) {
		transition = transitionForProgressInRange(progress, steps[0], steps[1]);
	}

	// If the progress is after the end of the range, extrapolate from the second last and last steps.
	else if (progress > (steps.length - 1)) {
		normalizedProgress = progressForValueInRange(progress, Math.floor(progress), Math.floor(progress)+1);
		normalizedProgress = normalizedProgress + 1;
		transition = transitionForProgressInRange(normalizedProgress, steps[(steps.length - 2)], steps[(steps.length - 1)]);
	}
	
	// Supress potential NaNs
	else if (progress == (steps.length - 1) || progress == 0) {
		transition = steps[progress];
	}
	
	// Otherwise interpolate between steps
	else {
		normalizedProgress = progressForValueInRange(progress, Math.floor(progress), Math.floor(progress)+1);
		transition = transitionForProgressInRange(normalizedProgress, steps[Math.floor(progress)], steps[Math.floor(progress)+1]);
	}
	
	return transition;
}